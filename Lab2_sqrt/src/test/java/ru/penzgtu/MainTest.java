package ru.penzgtu;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @org.junit.jupiter.api.Test
    void sqrtTrue() {
        assertEquals(3, Main.sqrt(9));
    }
    @Test
    void sqrtFalse() {
        assertNotEquals(1, Main.sqrt(9));
    }

}