package com.company;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    public void testSum() throws Exception{
        assertEquals(4,Main.sum(2,2), "Slojenie OK");
    }
    @Test
    public void testMin() throws Exception{
        assertEquals(5,Main.min(8,3), "Vichitanie OK");
    }
    @Test
    public void testUmn() throws Exception{
        assertEquals(4,Main.umn(2,2), "Umnojenie OK");
    }
    @Test
    public void testDel() throws Exception{
        assertEquals(5,Main.del(25,5), "Delenie OK");
    }

}