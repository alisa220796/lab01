package ru.penzgtu1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main  {

    public static void main(String[] args) {
        try(BufferedReader br = new BufferedReader(new FileReader("mbox.txt")))
        {
            //чтение построчно
            String s;
            Pattern pattern = Pattern.compile("\\w+([\\.-]?\\w+)*@\\w+([\\-.]?\\w+)*\\.\\w{2,4}");
            Matcher matcher;
            ArrayList<String> author = new ArrayList<String>();
            ArrayList <String> author_clear = new ArrayList<String>();

            while((s=br.readLine())!=null){
                if (s.contains("From:"))
                {
                    matcher = pattern.matcher(s);
                    if (matcher.find()) {
                        author.add(matcher.group());
                        //System.out.println(matcher.group());
                    }


                }

            }
            br.close();
            String[] author_mas=author.toArray(new String[author.size()]);
            Arrays.sort(author_mas);

            ArrayList<String> author_t=testt(author);
            System.out.println(author_t.size());

            int i=0;
            int j=0;
            ArrayList <Float> coefficient = new ArrayList<Float>();
            HashMap<String, Float> caef_for_user = new HashMap<>();
            HashMap<String, Integer> mail_for_user = new HashMap<>();
            ArrayList <Float> esche_coef = new ArrayList<Float>();

            s=null;
            String s1;
            String[] coe;
            int mail_c=0;


            while (j<author_t.size())
            {
                BufferedReader br1 = new BufferedReader(new FileReader("mbox.txt"));
                int calc=0;
                while((s=br1.readLine())!=null)
                {
                    if(s.equals("From: "+author_t.get(j))) {
                        while ((s1 = br1.readLine())!=null) {
                            if (s1.contains("X-DSPAM-Confidence:")) {
                                coe = s1.split(" ");
                                coefficient.add(Float.parseFloat(coe[1]));
                                mail_c++;
                                break;
                            }

                        }
                    }
                }
                if(mail_c!=0)
                {
                    float yahz = 0;
                    for (int ji = 0; ji < coefficient.size(); ji++) {
                        yahz += coefficient.get(ji);
                    }
                    float coef_hz = yahz / coefficient.size();
                    caef_for_user.put(author_t.get(j), coef_hz);
                    mail_for_user.put(author_t.get(j), mail_c);
                    mail_c = 0;
                    coef_hz=0;
                }


                j+=1;
                br1.close();
                coefficient.clear();
            }
            System.out.println(caef_for_user);
            System.out.println(mail_for_user);

        }

        catch(IOException ex){

            System.out.println(ex.getMessage());

        }
    }
    public static ArrayList<String> testt(ArrayList<String> author){
        for (int i=0; i<author.size(); i++)
        {
            int g=i+1;
            while (g<author.size())
            {
                if (author.get(i).equals(author.get(g)))
                {
                    author.remove(g);
                    g-=1;
                }
                g++;
            }
        }
        return author;
    }
}
